"use strict";

var Alexa = require("alexa-sdk");
var asia = require('./allCountries.json');

var handlers = {
  'LaunchRequest': function() {
    //this.attributes['numberCorrect'] = 0;
    this.attributes['revise'] = false;
    //this.attributes['reviseCorrect'] = 0;
    //this.attributes['myreviseScore'] = 0;
    this.attributes['currentRevise'] = 0;
    //this.attributes['playstreak'] = {};


    var dayIn;

    //this.attributes['userid'] = this.event.session.user.userId;
    this.attributes['timestamp'] = this.event.request.timestamp.split('T')[0];

    if(!this.attributes.playstreak == false){
      dayIn = Object.keys(this.attributes.playstreak).length

      var date = this.event.request.timestamp.split('T')[0];
      if(!this.attributes.playstreak[date] == true){
        this.attributes['initialSkip'] = true;
      }

    }
    else{
      var date = this.event.request.timestamp.split('T')[0];
      dayIn = 0;
      this.attributes.numberCorrect = 0;
      this.attributes['playstreak'] = {[date]:  0};
      this.attributes['reviseCorrect'] = 0;
      this.attributes['myreviseScore'] = 0;
      this.attributes['myreviseTimes'] = {[date]:  0};
    
    }

    this.response.speak(`Hello, Welcome to Capital Finder, I am Emma, Welcome to your ${dayIn+1}th time with me! I am a geography freak who loves\
      making new friends and learning more and more about our unique planet Earth everyday.<break time="0.7s"/>\
      Today I was planning to learn about various countries and their magnificent capitals, but a thought came across my mind, \
      Why get involved in this fun activity all alone, when I have friends to go along with. Yes I am talking about you! <break time="0.7s"/>\
      So shall we begin? Say play to start and say stop to end the game, you can always say reset to restart from the start! <break time="0.7s"/>
      You can say skip on every new day to start with new countries, if your previous score is more than 70 %. Best of luck!`).listen("Say play to start and say stop to end the game!").shouldEndSession(false);
    this.emit(':responseReady');
  },

  
  'startIntent': function() {
    
    var phrases = ["Awesome! correct answer, here comes the next country, ", "Bravo, Here comes the next country, ", 
      "Excellent, correct answer yet again! Let's move on to the next country, ",
      "Wow, correct answer, Here comes the next country, ",
      "Correct answer! Wow you are on a correct answer spree. Let's move on to the next country, "
    ];

    if(this.attributes['revise'] == false){


      if(this.attributes.numberCorrect == 0 || !(this.attributes.numberCorrect) == true) {

        var current = this.attributes['numberCorrect'];
        this.response.speak(`Okay, so lets begin our fun journey of learning capitals of various countries around the globe! But first a fun fact, ${asia[current].fact}! So say with me \
          capital of  ${asia[current].country}  is <break time="0.2s"/> ${asia[current].city}. <break time="3s"/> Let's do this again, but this time complete my sentence. \
          Capital of ${asia[current].country} is ?`)
                    .listen("Let's do this again, this time complete my sentence. Capital of "+ asia[current].country + " is ?").shouldEndSession(false);
        this.emit(':responseReady');  
      }
      else if (this.attributes.numberCorrect % 5 == 0 && this.attributes.numberCorrect != 0){
        this.response.speak("Horray! you are at a benchmark! You have come a long way, want a quick revision? Say revise to start revision or stop to end the game!")
                    .listen("Say revise to start revision or say stop to end the game!").shouldEndSession(false);
        this.emit(':responseReady');
      }
      else if (this.attributes.numberCorrect == 196){
        this.response.speak("Horray! another correct answer. You have reached the end! You have come a long way, want a quick revision? Say revise to start revision or say stop to end the game!")
                      .listen("Say revise to start revision or say stop to end the game!").shouldEndSession(false);
        this.emit(':responseReady');
      }
      else {
        var current = this.attributes['numberCorrect'];

        var phrase = phrases[Math.floor((Math.random() * (phrases.length)) )];
        
        this.response.speak( `${phrase} But first a fun fact, ${asia[current].fact}! So repeat after me, the capital of ${asia[current].country} is ${asia[current].city}. <break time="3s"/>\ 
        Now tell me what's the capital of  ${asia[current].country} ? `)
                    .listen().shouldEndSession(false);
        this.emit(':responseReady');  
      }


    }

    else{

      if(this.attributes.numberCorrect == 0){
        this.attributes.revise = false;
        this.response.speak(`I think we should once again learn the capitals of countries to score better. Say start to play again \
          or say stop to end the game.`)
                    .listen(`Say start to play again or say stop to end the game.`).shouldEndSession(false);
        this.emit(':responseReady');
      }

      if(this.attributes.reviseCorrect == 0) {

        var current = this.attributes['reviseCorrect'];
        this.response.speak(`Okay, so lets start revising capitals of various countries that we have learned so far! So tell me \
          what is the capital of  ${asia[current].country} ?`)
                    .listen("Let's do this again. Tell me what is the capital of "+ asia[current].country + " ?").shouldEndSession(false);
        this.emit(':responseReady');  
      }
  
      else if(this.attributes.reviseCorrect == this.attributes.numberCorrect){
        console.log(!this.attributes.myreviseTimes[currentDate]);
        
        var currentDate = this.event.request.timestamp.split('T')[0];

        if(!this.attributes['myreviseTimes'] == true && this.attributes['myreviseScore'] != 5 && this.attributes['myreviseScore'] % 5 != 0){
          this.attributes['myreviseTimes'] = {[currentDate]:  0};
        }
        else if(!this.attributes.myreviseTimes[currentDate] == true && this.attributes['myreviseScore'] != 0 && this.attributes['myreviseScore'] % 5 != 0){
          this.attributes['myreviseTimes'] =  {[currentDate]:  0};
        }
        else if(!this.attributes.myreviseTimes[currentDate] == true && this.attributes['myreviseScore'] != 0 && this.attributes['myreviseScore'] % 5 == 0){
          this.attributes['myreviseTimes'] =  {[currentDate]:  1};
          this.attributes['currentRevise']++;
        }
        else if(!this.attributes.myreviseTimes[currentDate] == false && this.attributes.myreviseTimes[currentDate] < 2*Object.keys(this.attributes.playstreak).length && this.attributes['myreviseScore'] != 0 && this.attributes['myreviseScore'] % 5 == 0){
          this.attributes.myreviseTimes[currentDate] ++;
          this.attributes['currentRevise']++;
        }
        else if(!this.attributes.myreviseTimes[currentDate] == false && this.attributes.myreviseTimes[currentDate] == 2*Object.keys(this.attributes.playstreak).length && this.attributes['myreviseScore'] != 0 && this.attributes['myreviseScore'] % 5 == 0){
          this.attributes.myreviseTimes[currentDate]  = 2*Object.keys(this.attributes.playstreak).length;
        }

        var myscore = (this.attributes.myreviseScore/this.attributes.reviseCorrect)*100;
        this.attributes['revise'] = false;
        //this.attributes['numberCorrect'] ++;

        if(!this.attributes['playstreak']){
          this.attributes['playstreak'] = {[currentDate]: myscore};
        }

        else {  // && currentDate in this.attributes['playstreak'] 
        this.attributes.playstreak[currentDate] =  myscore;
        
        }
        this.emit(':saveState', true);  
        this.response.speak(`You have successfully completed your revision, you scored ${myscore} %. To continue with the game say continue!\
          or say stop to end the game!`)
                    .listen("To continue with the game say continue, to revise again say revise or say stop to end the game!").shouldEndSession(false);
                    console.log(this.attributes.playstreak);
                      this.emit(':responseReady');
      }
      
      else if (this.attributes.reviseCorrect == 196){
        var myscore = (this.attributes.myreviseScore/this.attributes.reviseCorrect)*100;
        this.response.speak(`Horray! you got ${myscore} as final score. Thanks for playing! You have come a long way, Great job! Say reset to start again or say revise to revise again!`)
                      .listen(`Say reset to start again or say revise to revise again!`)
        this.emit(':responseReady');
      }
      else {
        var current = this.attributes['reviseCorrect'];
  
        if(this.attributes['lastreviseQuestion'] == 'correct'){
          var phrase = phrases[Math.floor((Math.random() * (phrases.length)) )];
          this.response.speak( `${phrase} Now tell me what's the capital of  ${asia[current].country} ? `)
                      .listen().shouldEndSession(false);
          this.emit(':responseReady');
        }
        
        else{
          this.response.speak( `Wrong answer, Now tell me what's the capital of  ${asia[current].country} ? `)
                      .listen().shouldEndSession(false);
          this.emit(':responseReady');
        }

      }
    }
  },


  'FindCapitalIntent': function () {
    if(this.attributes['revise'] == false){

      var current = this.attributes['numberCorrect'];
      var myCapital = this.event.request.intent.slots.namecapital.value;
      if (myCapital == asia[current].city) {
        this.attributes['numberCorrect']++;
        this.emit('startIntent');
        
      } else {
        this.response.speak(`Your guess of the capital being  ${myCapital} was not correct, but don't worry, let's give it one more try!\
          So repeat after me. Capital of ${asia[current].country} is ${asia[current].city}. <break time="3s"/> Now complete my sentence. Capital\
            of ${asia[current].country} is ?`)
                    .listen("Lets give it another try! Okay, so what is the capital of "+ asia[current].country + " ?").shouldEndSession(false);
      }
      this.emit(':responseReady');
    }


    else{

      var current = this.attributes['reviseCorrect'];
      var myCapital = this.event.request.intent.slots.namecapital.value;
      if (myCapital == asia[current].city) {
        //this.response.speak(`Your answer is correct!  ${asia[current].city} is the capital of ${asia[current].country} .`);
        this.attributes['reviseCorrect']++;
        this.attributes['myreviseScore']++;
        this.attributes['lastreviseQuestion'] = 'correct';
        
      } else {
        //this.response.speak(`Your guess of the capital being  ${myCapital} was not correct, The correct answer is ${asia[current].city}.`);
        this.attributes['reviseCorrect']++;
        this.attributes['lastreviseQuestion'] = 'incorrect';
      }
      this.emit('startIntent');
    }

  },


 'AMAZON.HelpIntent' : function () {
    var myphrases = ["It's completely fine", "No need to worry", "Don't worry"];
    var myphrase = myphrases[Math.floor((Math.random() * (myphrases.length)) )];
    var current = this.attributes['numberCorrect'];
    this.response.speak(`${myphrase}. We will do this again. So repeat after me, capital of ${asia[current].country} is ${asia[current].city}. <break time="3s"/>\ 
      Now tell me what's the capital of  ${asia[current].country} ? `)
                .listen().shouldEndSession(false);
    this.emit(':responseReady');
 },

  'skipIntent': function () {
    var date = this.event.request.timestamp.split('T')[0];

    if(this.attributes.myreviseTimes[date] == 2*Object.keys(this.attributes.playstreak).length && this.attributes.currentRevise != 0 && this.attributes.currentRevise % 2 == 0){
      this.attributes.myreviseTimes[date] = 0;
      this.attributes.currentRevise = 0;
      this.attributes['revise'] = false;
      //this.attributes['numberCorrect'] = 0;
      this.response.speak(`I know you are having fun and are eager to learn more, so am I ! But as it is said, slow and steady always\
        wins the race. So come back tomorrow to get back on our learning spree! You can say start to learn again,\
        or stop to end the game!`)
                  .listen().shouldEndSession(false);
    }
    else if(this.attributes.myreviseTimes[date] == 2*Object.keys(this.attributes.playstreak).length && this.attributes.numberCorrect % 10 == 0 && this.attributes.currentRevise != 0){
      this.attributes.myreviseTimes[date] = 0;
      this.attributes['revise'] = false;
      //this.attributes['numberCorrect'] = 0;
      this.response.speak(`I know you are having fun and are eager to learn more, so am I ! But as it is said, slow and steady always\
        wins the race. So come back tomorrow to get back on our learning spree! You can say start to learn again,\
        or stop to end the game!`)
                  .listen().shouldEndSession(false);
    }

    else if( this.attributes.myreviseTimes[date] != 2*Object.keys(this.attributes.playstreak).length && !this.attributes.playstreak[date] == false  && this.attributes.playstreak[date] >= 70 && this.attributes.numberCorrect +1 <= 10 * Object.keys(this.attributes.playstreak).length ){ 
      this.attributes['revise'] = false;
      var current = this.attributes['numberCorrect'];

      var date = this.event.request.timestamp.split('T')[0];
      this.attributes.playstreak[date] = 0;

      this.response.speak(`Okay, so lets get back to gaining knowledge! But first a fun fact, ${asia[current].fact}. So repeat after me, capital of ${asia[current].country} is ${asia[current].city}. <break time="3s"/>\ 
      Now tell me what's the capital of  ${asia[current].country} ? `)
                  .listen().shouldEndSession(false);
    }

    else if( this.attributes.initialSkip == true && this.attributes.myreviseTimes[date] != 2*Object.keys(this.attributes.playstreak).length  && !this.attributes.playstreak[date] == true && this.attributes.playstreak[Object.keys(this.attributes.playstreak)[Object.keys(this.attributes.playstreak).length-1]] >= 70){ 
      this.attributes['revise'] = false;
      this.attributes['initialSkip'] = false;
      var current = this.attributes['numberCorrect'];
      this.response.speak(`Okay you scored good in the last revision, so lets get back to gaining knowledge! So repeat after me, capital of ${asia[current].country} is ${asia[current].city}. <break time="3s"/>\ 
      Now tell me what's the capital of  ${asia[current].country} ? `)
                  .listen().shouldEndSession(false);
    }

    else if(this.attributes.playstreak[date] < 70 || this.attributes.playstreak[Object.keys(this.attributes.playstreak)[Object.keys(this.attributes.playstreak).length-1]] < 70) {

      this.attributes['revise'] = false;
      this.attributes['numberCorrect'] = 0;
      this.response.speak(`I know you are having fun and are eager to learn more, so am I ! But as it is said, You can't build a great building\
        on a weak foundation. You must have a solid foundation if you're going to have a strong superstructure. So try to get more than 70 %\
        in the revision quiz today to get back on our learning spree! Say start to learn again, or stop to end the game!`)
                  .listen().shouldEndSession(false);  
    }
    
    else{
      this.attributes.myreviseTimes[date] = 0;
      this.attributes.currentRevise = 0;
      this.attributes['revise'] = false;
      //this.attributes['numberCorrect'] = 0;
      this.response.speak(`You can say start to learn again, or stop to end the game!`)
                  .listen().shouldEndSession(false); 
    }
    this.emit(':responseReady');
  },

  'reviseIntent': function () {
    var date = this.event.request.timestamp.split('T')[0];

    if(this.attributes.myreviseTimes[date] != 0 && (this.attributes['currentRevise'] > this.attributes.myreviseTimes[date] ||  this.attributes.myreviseTimes[date] == 2*Object.keys(this.attributes.playstreak).length && this.attributes.playstreak[Object.keys(this.attributes.playstreak)[Object.keys(this.attributes.playstreak).length-1]] >= 70 )){

      this.attributes.revise = false;
      this.attributes['currentRevise'] = 0;
      this.attributes['numberCorrect'] = 0;
      this.response.speak(`I know you are having fun and are eager to learn more, so am I ! But as it is said, slow and steady always\
          wins the race. So come back tomorrow to get back on our learning spree! Say start to learn again, or stop to end the game!`)
                    .listen();
    }

    else{
      //this.attributes['numberCorrect'] = 0;
      this.attributes.revise = true;
      this.attributes['reviseCorrect'] = 0;
      this.attributes['myreviseScore'] = 0;
      this.emit('startIntent');
    }
  },

  'restartIntent': function () {
    var date = this.event.request.timestamp.split('T')[0];
    this.attributes['currentRevise'] = 0;
    this.attributes['numberCorrect'] = 0;
    this.attributes['revise'] = false;
    this.attributes['reviseCorrect'] = 0;
    this.attributes['myreviseScore'] = 0;
    this.attributes['playstreak'] = {[date]:  0};
    this.attributes['myreviseTimes'] = {[date]:  0};
    
    this.emit('LaunchRequest');
  },

  'AMAZON.StopIntent': function () {
    this.response.speak("You are improving! Nice work. Goodbye for now!");
    this.emit(':saveState', true);
    this.emit(':responseReady');
  },

  'Unhandled': function () {
    this.response.speak("That didn't seem to be a valid response. Should we continue with the game? Say play to continue or say stop to end!")
                  .listen("Should we continue with the game? Say play to continue!").shouldEndSession(false);
    this.emit(':responseReady');
  }
};

exports.handler = function(event, context, callback){
    var alexa = Alexa.handler(event, context);
    //alexa.appId="amzn1.ask.skill.8401bcf7-a8de-4414-ba55-cd58665e8281";
    alexa.appId = "amzn1.ask.skill.8f1c1a54-06cd-4cb1-b24b-3d21ba23bfca";
    alexa.dynamoDBTableName = 'Capitals';
    alexa.registerHandlers(handlers);
    alexa.execute();
};
